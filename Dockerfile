FROM alpine

RUN apk --update add npm firefox 

RUN npm install -g selenium-side-runner

RUN npm install -g geckodriver

WORKDIR /app